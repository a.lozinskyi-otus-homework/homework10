import React, {useState} from "react";
import { MDBInput, MDBBtn } from "mdbreact";
import {useSelector, useDispatch} from 'react-redux';
import userActions from '../redux-hooks/userActions'

function Login() {
    const [login, setLogin] = useState();
    const [password, setPassword] = useState();

    const currentUser = useSelector(state => state.currentUser);
    const dispatch = useDispatch();

    const handleLogin = () => {
        dispatch(userActions.setUser({ login : login }));
    };

    const handleLogout = () => {
        dispatch(userActions.logOut());
    };

    return(
        <div className="form-group">
            <h1>Войти в приложении</h1>
            <MDBInput label="login" size="sm" value={login} onChange={e => setLogin(e.target.value)} />
            <MDBInput type="password" label="password" size="sm" value={password} onChange={e => setPassword(e.target.value)} />
            {!currentUser.loggedIn &&
            <MDBBtn onClick={handleLogin}>
                    Войти
            </MDBBtn>
            }
            
            {currentUser.loggedIn && 
                <MDBBtn onClick={handleLogout}>
                    Выйти        
                </MDBBtn>
            }
            <MDBInput type="textarea" rows="5" disabled value={currentUser.loggedIn ? "Вы вошли как " + currentUser.user.login : "Залогиньтесь"} />
        </div>
    )
}

export default Login;