import React from "react";
import PropTypes from "prop-types";
import { MDBInput, MDBBtn } from "mdbreact";

const axios = require('axios');

export default class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            postid: this.props.postid
        };
    }
    
    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    
    handleSubmit = (event) => {
        event.preventDefault();
        axios.get('posts/' + this.state.postid)
            .then((response) => {
                this.setState({
                    post: response.data
                })
            })
            .catch((error) => {
                this.setState({
                    post: null
                })              
            });
    }

    render() {
        return (
            <div>
                <h1>Поиск постов</h1>
                <div>
                    <MDBInput
                        id="postid"
                        name="postid"
                        label="Id поста"
                        value={this.state.postid}
                        onChange={this.handleChange}
                    />

                    <MDBBtn
                        variant="contained" 
                        color="primary" 
                        onClick={this.handleSubmit}>
                    Найти пост        
                    </MDBBtn>
                </div>
                <div>
                    <MDBInput
                        disabled={true}
                        name="Id"
                        label="Id"
                        value={this.state.post ? this.state.post.id : ''}/>

                    <MDBInput 
                        disabled={true}
                        name="Title"
                        label="Title"
                        value={this.state.post ? this.state.post.title : ''}/>

                    <MDBInput 
                        disabled={true}
                        name="Author" 
                        label="Author"
                        value={this.state.post ? this.state.post.author : ''}/>
                </div>
            </div>
      )
    }
}

Posts.propTypes = {
    postId: PropTypes.number,
}