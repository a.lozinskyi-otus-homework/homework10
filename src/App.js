import './App.css';
import Posts from './Components/Posts';
import Login from './Components/Login';
import NoMatch from './Components/NoMatch';
import Home from './Components/Home';


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <Router>
      <h1>Стартовая страница</h1>
      <div>
        <nav>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/posts">Posts</Link>
          </li>
        </nav>
      </div>
      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/posts">
          <Posts />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="*">
          <NoMatch />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
